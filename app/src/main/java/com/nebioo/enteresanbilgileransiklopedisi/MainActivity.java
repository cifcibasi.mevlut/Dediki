package com.nebioo.enteresanbilgileransiklopedisi;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    private SozKitabi sozKitabi = new SozKitabi();
    private RenkAtlasi renkAtlasi = new RenkAtlasi();

    private TextView dedikiTextView;
    private Button digerButton;
    private RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dedikiTextView = (TextView) findViewById(R.id.dedikiTextView);
        digerButton = (Button) findViewById(R.id.digerButton);
        relativeLayout= (RelativeLayout) findViewById(R.id.relativeLayout);

        digerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String soz = sozKitabi.getSoz();

                dedikiTextView.setText(soz);

                int renk = renkAtlasi.getRenk();
                relativeLayout.setBackgroundColor(renk);
                digerButton.setTextColor(renk);
            }
        });

        Log.d(TAG,"MainActivity");
    }
}
