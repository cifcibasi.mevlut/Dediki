package com.nebioo.enteresanbilgileransiklopedisi;

import java.util.Random;

/**
 * Created by mevlüt çifcibaşı on 4.02.2018.
 */

class SozKitabi {

    private String[] sozler = {
            "Dedi ki aslında her şey ayan, yeter ki sen uyan...",
            "Dedi ki kötüye acımak iyiye zulümdür.",
            "Dedi ki nimeti fark etmek şükürdür.",
            "Dedi ki çalışmak değil çalışmamak tüketir.",
            "Dedi ki büyük adam olmak iyi adam olmaktan kolaydır.",
            "Dedi ki sadece versin diye isteme, esas kulluk gereği için iste.",
            "Dedi ki kursağına düşeni değil, gönlüne düşeni kar say.",
            "Dedi ki hatırlıyorsan hatırı olduğundandır.",
            "Dedi ki fedakarlığın başkaları için yaptıklarınla değil, kendin için yapmadıklarınla ölçülür.",
            "Dedi ki hatasız kul olmaz, ya tövbesiz? Kul olmaz.",
            "Dedi ki taatın meşakkati gider sevabı kalır, günahın lezzeti gider acısı kalır.",
            "Dedi ki aşk makamı; minber değil darağacıdır.",
            "Dedi ki mürekkebin akmadığı yerde kan akar.",
            "Dedi ki kötü niyetliden değil kötü niyetini gizleyenden kork.",
            "Dedi ki iyiler kaybetmez, kaybedilir.",
            "Dedi ki manevi enerji isteyen yansın.",
            "Dedi ki marifeti çok olanın hüznü de çoktur.",
            "Dedi ki güzelliğin ne kadarsa nazın da o kadar olsun.",
            "Dedi ki Hakkı bulmak kolaydır, çünkü şahidi ve delili çoktur; zor olan Hak ile halka dönmektir.",
            "Dedi ki beni görmen rü`yet, niyetimi görmen basiret, akıbetimi görmen nazardır."
    };

    String getSoz(){

        Random randomGenerator = new Random();
        int randomNumber = randomGenerator.nextInt(sozler.length);

        return sozler[randomNumber];
    }
}
